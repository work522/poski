// Menu JS
const menuBtn = document.querySelector(".menu-btn");
const nav = document.querySelector(".nav");
const closeBtn = document.querySelector(".menu-btn__close");

menuBtn.addEventListener("click", toggleMenu);
closeBtn.addEventListener("click", toggleMenu);

let showMenu = false;

function closeMenu() {
  nav.classList.remove("open");
  showMenu = false;
}

function toggleMenu() {
  if (!showMenu) {
    nav.classList.add("open");

    showMenu = true;
  } else {
    closeMenu();
  }
}

// Tuto funkci jsem byl schopen napsat az s pomoci internetu, closest a contains mi byly naprosto
// cizi.
document.addEventListener("click", function (e) {
  const clickElement = e.target;
  if (
    clickElement.classList.contains(".menu-btn") ||
    clickElement.closest(".menu-btn")
  ) {
    return;
  }
  if (!clickElement.closest(".nav.open")) {
    closeMenu();
  }
});

document.addEventListener("keydown", function (e) {
  if (e.key.toLowerCase() === "escape" && showMenu) {
    toggleMenu();
  }
});

// 1. JS zadani
let numbArts = document.querySelectorAll(".arts");
console.log(`Počet příspěvků v projektu: ${numbArts.length}`);

// 2. JS zadani
// getBoundingClientRect jsem dohledal pomoci internetu, neznal jsem to predtim
const box = document.querySelector("section.pictures");
const rect = box.getBoundingClientRect();
const avgDiv = (a, b) => a / b;

console.log(
  `Průměrná výška příspěvků je:`,
  avgDiv(rect.height, numbArts.length)
);

//3. JS zadani
let titles = document.querySelectorAll(".titles");
let totalLength = 0;

for (const title of titles) {
  totalLength += title.textContent.length;
}

console.log(`Délka nadpisů všech příspěvků je: ${totalLength}`);

// 4. JS zadani
document.querySelectorAll(".article").forEach((article) => {
  article.addEventListener("click", () => {
    console.log(article.textContent);
  });
});

// 5. JS zadani
// Scroll jsem jeste taky nikdy nepouzil, tak jsem si nasel jen na netu addEventListener jestli existuje scroll
const el = document.querySelector(".icons");

window.addEventListener("scroll", function () {
  console.log(`Shora: ${el.getBoundingClientRect().top}`);
  console.log(`Zleva: ${el.getBoundingClientRect().left}`);
});
